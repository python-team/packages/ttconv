Source: ttconv
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Martin <debacle@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools,
 python3-pytest <!nocheck>
Standards-Version: 4.7.0
Homepage: https://github.com/sandflow/ttconv
Vcs-Git: https://salsa.debian.org/python-team/packages/ttconv.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/ttconv
Rules-Requires-Root: no

Package: python3-ttconv
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends}
Description: library and tool for converting timed text or subtitle formats
 ttconv is a library and command line application written in pure
 Python for converting between timed text formats used in the
 presentations of captions, subtitles, karaoke, etc.
 .
 Input formats: TTML / IMSC, SCC / CEA 608, EBU STL, SRT
 .
 Output formats: IMSC / TTML, WebVTT, SRT
 .
 Note, that the tool is called ttconv, not tt, i.e. use it like this:
 .
 ttconv convert -i subtitles.ttml -o subtitles.srt
